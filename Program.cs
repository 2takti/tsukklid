﻿using System;
using System.Collections.Generic;

namespace enne_tükkleid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int arv = 77;                     //see on muutuja - int tüüpi

            int[] arvud = new int[10];        // see on muutuja - palju int tüüpi asju
                                              // massiiv - array - hetkel 10 arvu

            int[] teised = arvud;
            teised[4] = 44;


            Console.WriteLine(++arv);         //tegemist on unaartehtega - ainult üks operand. töötab ainult arvu tüüpi muutujaga / 
                                              // arv++; // peaaegu sama, mis arv += 1
                                              // arv--; // peaaegu sama, mis arv -= 1
                                              // arv = 10;     ----------------------------------------------------------------pmst kõik see on et suurendada või vähendada ühe võrra
                                              // Console.WriteLine(arv++); // trükib 10, aga a väärtus on 11
                                              // Console.WriteLine(++arv); // trükib 12 ja a väärtus on 12
                                              // arvud[7]++


            arvud[3] = 8;                    // omistame 4.le arvule väärtuse 8
            Console.WriteLine(arvud[3]);     // trükime 4. arvu välja
            // massiivi elemendid saavad omale vaikimisi seda null-väärtuse

            //masiivi algväärtustamine (kui vaja)
            //kolm varianti - samaväärsed

            int[] arvud2 = new int[5] { 1, 2, 3, 7, 20 };
            int[] arvud3 = new int[] { 1, 2, 3, 7, 20 };
            int[] arvu4 = { 1, 2, 3, 7, 20 };

            Console.WriteLine(arvud4.Length);   // massiiivi

            DateTime[] sünnipäevad =
            {
                new DateTime(1950,4,6) ,
                new DateTime(1980,2,4) ,
                new DateTime(1994,2,20)
            };

            //----------------- paar keerulisemat asja:

            int[,] tabel = new int[10, 7];   // see on 2-mõõtmeline massiiv 10-rida, 7 veergu
            int[,] tabel2 = { { 1, 2, 3 }, { 4, 5, 6 } }; //2 rida 3 veergu

            Console.WriteLine(tabel[1, 3]);    //2. rea 4. veerg
            //sellist asja paljudes teistes keeltes (ntx java) ei ole

            //massiivide massiiv - koosneb massividest
            int[][] segu = new int[3][];
            segu[0] = new int[4]; // iga massiiv selle reas tuleb eraldi  tekitada 
            segu[1] = new int[7];
            segu[1][4] = 17;

            int[][] segu2 = { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6, 7 } };

            int[,,] kuubik = { { { 1, 2, 3 }, { 4, 5, 6 } }, { { 7, 8, 9 }, { 10, 11, 12 } } };

            int[][][] jora;  // massiivides koosnev massiiv- tõsine jora

            Console.WriteLine(kuubik.Lenght);   // see ütleb , kui palju 3-mõõtmelisi üksikuid lahtreid on

            Console.WriteLine(segu2.Length);    // see ütleb kaks
            Console.WriteLine(segu2[0].Lenght);  //see ütleb, et 3 : new int [] { 1, 2, 3 }

            Console.WriteLine(kuubik.Rank);
            // see ütleb, palju on mõõtmeid (hetkel 3, lihtmassiivil 1)
            Console.WriteLine(kuubik.GetLength(0));   // mitu 0 kihti, 1 rida, 2 veergu 
            Console.WriteLine(kuubik.GetLength(1));   // mitu 0 kihti, 1 rida, 2 veergu
            Console.WriteLine(kuubik.GetLength(2));   // mitu 0 kihti, 1 rida, 2 veergu

            //selleks lisame üles uue rea - using System.Collection.Generic;
            // kollektsioonidega (massiivid on lihtsalt üks liik neid) maadleme edaspidigi

            List<int> rida = new List<int>();   //tegemist oleks kahh nagu massiviga
                                                // aga selle liikmete arv on muutuv

            rida.Add(77); // {77}
            rida.Add(88); // {77 ,88}
            Console.WriteLine(rida[0]);    // trükitakse 77
            rida.Add(99);  // {77, 88, 99}
            Console.WriteLine(rida[1]);   // trükitakse 88
            rida.Remove(88); // {77,99}
            Console.WriteLine(rida[1]); // trükitakse 99

            List<int> rida2 = new List<int>(); //tekitan tühja listi----- ilma initsializerita
            List<int> rida3 = new List<int>() { 1, 77, 2, 33 };   // tekitan listi, kus on ees 4 asja
            List<int> rida4 = new List<int> { 1, 77, 2, 33 };     // tekitan listi kus on ees 4 asja
            List<int> rida4 = new List<int> { };      // tekitan tühja listi----

            for (int 1 = 0; int < 20; i++)
            {
                rida2.Add(i);
                Console.WriteLine($"rivis on {rida2.Count} ja sinna mahub {rida2.Capacity}");
            }


            //List-il on kolm põhilist tehet
            //List<int> rida = new List>int<();
            //rida.Add(77);     paneb rivi lõppu uue arvu
            //rida.Remove(77);   otsib rivist 77 ja eemaldab selle
            //rida.RemoveAt(4);   eemaldab rivist 5nda tegelase

            //List-IList ei ole length - listil on Count
            //rida.Count - ütleb palju mul on listis asju
            //rida[3] = 100;      listi 4. (neljas) element saab 100
            //rida[3]++;
            //Console.WriteLine(rida[3]);
            //NB  index peab olema >= 0 ja < Count



        }
    }
}
